# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Vit Pelcak <vit@pelcak.org>, 2013, 2015, 2017, 2018, 2019, 2020, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:47+0000\n"
"PO-Revision-Date: 2023-02-21 09:38+0100\n"
"Last-Translator: Vit Pelcak <vit@pelcak.org>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 22.12.2\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#: editorpage.cpp:52
#, kde-format
msgid "Theme Templates:"
msgstr "Šablony motivů:"

#. i18n: ectx: Menu (edit)
#: headerthemeeditorui.rc:16
#, kde-format
msgid "&Edit"
msgstr "&Upravit"

#. i18n: ectx: Menu (Display)
#: headerthemeeditorui.rc:22
#, kde-format
msgid "&Display"
msgstr "&Zobrazení"

#: main.cpp:37
#, kde-format
msgid "Header Theme Editor"
msgstr ""

#: main.cpp:39
#, kde-format
msgid "Messageviewer Header Theme Editor"
msgstr ""

#: main.cpp:41
#, kde-format
msgid "Copyright © 2013-%1 headerthemeeditor authors"
msgstr "Autorská práva © 2013-%1 Vývojáři headerthemeeditor"

#: main.cpp:42
#, kde-format
msgid "Laurent Montel"
msgstr "Laurent Montel"

#: main.cpp:42
#, kde-format
msgid "Maintainer"
msgstr "Správce"

#: previewwidget.cpp:34 themeeditormainwindow.cpp:136
#, kde-format
msgid "Update view"
msgstr "Aktualizovat pohled"

#: themeconfiguredialog.cpp:27
#, kde-format
msgctxt "@title:window"
msgid "Configure"
msgstr "Nastavit"

#: themeconfiguredialog.cpp:37
#, kde-format
msgid "Default email:"
msgstr ""

#: themeconfiguredialog.cpp:42
#, kde-format
msgid "General"
msgstr "Obecné"

#: themeconfiguredialog.cpp:45
#, kde-format
msgid "Default Template"
msgstr "Výchozí šablona"

#: themedefaulttemplate.cpp:14
#, kde-format
msgid "Subject"
msgstr "Předmět"

#: themedefaulttemplate.cpp:21
#, kde-format
msgid "From"
msgstr "Od"

#: themedefaulttemplate.cpp:28
#, kde-format
msgid "To"
msgstr "Komu"

#: themedefaulttemplate.cpp:35
#, kde-format
msgid "Cc"
msgstr "Kopie"

#: themeeditormainwindow.cpp:79
#, kde-format
msgid "Load Recent Theme..."
msgstr "Načíst nedávný motiv..."

#: themeeditormainwindow.cpp:86
#, kde-format
msgid "Add Extra Page..."
msgstr "Přidat extra stránku..."

#: themeeditormainwindow.cpp:91
#, kde-format
msgid "Upload theme..."
msgstr "Odeslat motiv..."

#: themeeditormainwindow.cpp:97
#, kde-format
msgid "New theme..."
msgstr "Nový motiv..."

#: themeeditormainwindow.cpp:100
#, kde-format
msgid "Open theme..."
msgstr "Otevřít motiv..."

#: themeeditormainwindow.cpp:102
#, kde-format
msgid "Save theme"
msgstr "Uložit téma"

#: themeeditormainwindow.cpp:105
#, kde-format
msgid "Save theme as..."
msgstr "Uložit motiv jako..."

#: themeeditormainwindow.cpp:111
#, kde-format
msgid "Install theme"
msgstr "Instalovat motiv"

#: themeeditormainwindow.cpp:115
#, kde-format
msgid "Insert File..."
msgstr "Vložit soubor..."

#: themeeditormainwindow.cpp:121
#, kde-format
msgid "Printing mode"
msgstr "Režim tisku"

#: themeeditormainwindow.cpp:126
#, kde-format
msgid "Normal mode"
msgstr "Normální režim"

#: themeeditormainwindow.cpp:132
#, kde-format
msgid "Manage themes..."
msgstr "Spravovat motivy..."

#: themeeditormainwindow.cpp:214 themeeditormainwindow.cpp:347
#, kde-format
msgid "Select theme directory"
msgstr "Vyberte adresář motivu"

#: themeeditormainwindow.cpp:230
#, kde-format
msgid "Directory does not contain a theme file. We cannot load theme."
msgstr ""

#: themeeditorpage.cpp:37
#, kde-format
msgid "Editor (%1)"
msgstr "Editor (%1)"

#: themeeditorpage.cpp:46
#, kde-format
msgid "Desktop File"
msgstr "Soubor plochy"

#: themeeditorpage.cpp:157
#, kde-format
msgid "Theme already exists. Do you want to overwrite it?"
msgstr "Motiv již existuje. Přejete si jej přepsat?"

#: themeeditorpage.cpp:158
#, kde-format
msgid "Theme already exists"
msgstr "Motiv již existuje"

#: themeeditorpage.cpp:166
#, kde-format
msgid "Cannot create theme folder."
msgstr ""

#: themeeditorpage.cpp:177
#, kde-format
msgid "Theme installed in \"%1\""
msgstr ""

#: themeeditorpage.cpp:198
#, kde-format
msgid "We cannot add preview file in zip file"
msgstr ""

#: themeeditorpage.cpp:198
#, kde-format
msgid "Failed to add file."
msgstr "Selhalo přidání souboru."

#: themeeditorpage.cpp:212
#, kde-format
msgid "My favorite KMail header"
msgstr ""

#: themeeditorpage.cpp:235
#, kde-format
msgid "Filename of extra page"
msgstr ""

#: themeeditorpage.cpp:235
#, kde-format
msgid "Filename:"
msgstr "Název souboru:"

#: themeeditorpage.cpp:282
#, kde-format
msgid "Do you want to save current project?"
msgstr ""

#: themeeditorpage.cpp:283
#, kde-format
msgid "Save current project"
msgstr "Uložit současný projekt"

#: themetemplatewidget.cpp:34
#, kde-format
msgid "You can drag and drop element on editor to import template"
msgstr ""
